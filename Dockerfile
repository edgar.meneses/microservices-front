FROM node:10

COPY ["package.json",  "/usr/src"]


WORKDIR /usr/src

RUN npm install

RUN npm install -g nodemon

COPY [".", "/usr/src"]

EXPOSE 4000

CMD ["node", "bin/www"]