const URL = process.env.API_SERVER || "http://localhost:3000/api"
module.exports = {
    "getUsers":URL+"/users/list",
    "newUser":URL+"/users/new",
    "getUser":URL+"/users/find/",
    "updateUser":URL+"/users/update/",
    "deleteUser":URL + "/users/delete/"
}