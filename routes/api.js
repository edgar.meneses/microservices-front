const fetch = require("node-fetch");


module.exports.getUsers = async function (url) {
    return getHttpRequest(url);
};

module.exports.newUser =  async function (url, data) {
    return otherHttpRequest(url,'POST', data);
};

module.exports.getUser = async function (url) {
    return getHttpRequest(url);
};

module.exports.updateUser = async function (url, data) {
    return otherHttpRequest(url,'PUT', data)
};

module.exports.delete = async function(url) {
    return otherHttpRequest(url,'DELETE', {});
}

let getHttpRequest = async function (url) {
    try {
        let response = await fetch(url);
        let json = await response.json();

        return json;
    } catch (err) {
        return {
            success:false,
            message:'No fue posible procesar la petición',
            trace: err,
            code: "ERG"
        };
    }
};

let otherHttpRequest = async function (url, method, data) {
    try {
        let options = {
            method: method,
            headers: {
                'Content-type':'application/json'
            },
            body:JSON.stringify(data)
        }

        let response = await fetch(url, options);
        let json = await response.json();

        return json;

    }catch (err) {
        return {error: err}
    }
};