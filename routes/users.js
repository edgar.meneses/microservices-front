const express = require('express');
const router = express.Router();
const api = require('./api');
const endPoints = require('../config/api');


/* GET users listing. */
router.get('/',async (req, res) => {
 
  let users = await api.getUsers(endPoints.getUsers);
  console.log('Users ', users);
  res.render("users/users", {users:users.data});

});

router.get('/new', (req, res) => {
  res.render('users/new');
});

router.post('/new', async (req, res) => {

  let datos = {
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      email : req.body.email,
      gender : req.body.gender,
      documentType: req.body.documentType,
      document: req.body.document
  };

  let save = await api.newUser(endPoints.newUser, datos);

  if(save.success) {
      res.redirect('/users');
  }else {
      res.redirect('/users');
  }
});

router.get('/update/:id', async (req, res) => {
    let user = await api.getUser(endPoints.getUser+req.params.id);
    res.render('users/new', {user:user.data, updating:true})
});

router.post('/update/:id', async (req, res) => {
    let data = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        gender: req.body.gender
    };

    let update = await api.updateUser(endPoints.updateUser+req.params.id, data);

    if(update.success) {
        res.redirect('/users');
    }else {

        res.redirect('/users');
    }

});

router.get('/delete/:id', async (req, res) => {
    let update = await api.delete(endPoints.deleteUser+req.params.id);

    if(update.success) {
        res.redirect('/users');
    }else {
        res.redirect('/users');
    }

});

router.get('/profile/:id', async (req, res ) => {
    let user = await api.getUser(endPoints.getUser+req.params.id);
    res.render('users/profile',{user:user.data});
});

module.exports = router;
